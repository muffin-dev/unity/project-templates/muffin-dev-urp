# Muffin Dev for Unity - Universal Render Pipeline project template - Changelog

## [1.0.0] - 2020-11-05
### Added
- First version!

## [1.1.0] - 2020-11-23
### Added
- Include *Muffin Dev **Core Library*** standalone package

## [1.2.0] - 2021-4-23
### Changes
- Update packages to target Unity 2020.3.0
### Fixes
- Fix documentation links

## [1.3.0] - 2021-9-27
### Changes
- Update packages to target Unity 2020.3.19