# Muffin Dev for Unity - Universal Render Pipeline project template

This repository contains a Unity project template that uses the *Universal Render Pipeline*.

## Install template

This guide will show you how to setup this project template, and so, create new projects with this template directly from Unity Hub.

First, download the `com.muffindev.template.urp.tgz` file, and put it in your Unity installation directory at:

```txt
Path to your Unity installation/Editor/Data/Resources/PackageManager/ProjectTemplates
```

***TIP**: You can find your Unity installation folders from *Unity Hub*. In the "Installs" tab, click on the "More" icon of one of your install, then select "Show In Explorer".*

... **That's it!** Open Unity Hub (or run the Unity version where you just put the file), and try creating a new project with your new template!

![*Unity Hub* preview with Muffin Dev URP template](./package/Documentation~/unity-hub-preview.jpg)

Note that if Unity Hub was opened when you added the archive, you have to restart it before seing the changes.

## Content

### Included Unity packages (from the Package Manager)

- [2D Common](http://docs.unity3d.com/Packages/com.unity.2d.common@2.0)
- [Input System](https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0)
- [Unity UI](https://docs.unity3d.com/Packages/com.unity.ugui@1.0)
- [Universal RP](https://docs.unity3d.com/Packages/com.unity.ugui@1.0)
- [Visual Studio Code Editor](https://docs.unity3d.com/Packages/com.unity.ide.vscode@1.2)
- [Rider Editor](http://docs.unity3d.com/Packages/com.unity.ide.rider@1.1)

### Included *Muffin Dev* packages

- [Core Library (standalone)](https://gitlab.com/muffin-dev/unity/standalone/core-library)

### Graphic Assets

The graphic assets are bundled as `*.zip` files in the `/_GRAPH` directory of the project.

Most of these graphic assets (in the `/_GRAPH` directory) come from [Kenney's packs (thanks again for your awesome work!)](https://www.kenney.nl/assets). The 3D characters and their animations come from [Mixamo](https://www.mixamo.com). Fonts come from [Dafont](https://www.dafont.com).

Note that all the included assets are dedicated to the public domain, so you can use them in your projects even for commercial use.

Here is the list of the used asset packs from [Kenney](https://www.kenney.nl/assets):

- [UI Pack](https://www.kenney.nl/assets/ui-pack)
- [Game Icons](https://www.kenney.nl/assets/game-icons)
- [Platformer Kit (3D)](https://www.kenney.nl/assets/platformer-kit)
- [Scribble Platformer (2D)](https://www.kenney.nl/assets/scribble-platformer)
- [Particle pack](https://www.kenney.nl/assets/particle-pack)

### Get more assets to bootstrap your project

Here is a short list of free resources you can get for your game:

- [Kenney](https://www.kenney.nl): 3D, 2D, UI, sounds (and also awesome games and tools if you have some dollars to spare!)
- [Mixamo](https://www.mixamo.com): 3D characters, animations
- [Dafont](https://www.dafont.com): Fonts
- [Google Fonts](https://fonts.google.com): Fonts
- [Material Icons (Google)](https://material.io/resources/icons): Icons
- [OpenGameArt](https://opengameart.org): Everything
- [Game Art 2D](https://www.gameart2d.com/freebies.html): 2D spritesheets for characters and UI
- [Universal Soundbank](http://www.universal-soundbank.com): SFX, sounds and musics
- ...

Do you know other free resources that could be part of this list? Please let me know at [contact@muffindev.com](mailto:contact@muffindev.com)!

### Utilities

This project template already contains a `*.gitignore` file, so you can initialize *Git* in your project without worryig about which files you shoud add or not to your repository.

## Known limitations

You need to put the project template `*.tgz` file in Unity installs for each version where you want to use it (and so, repeat the process each time you install a new Unity version).

## Support the creators!

This project include my library (which is open source), but also include awesome assets made by great creators. Even if you can use all of it for free, consider donate to support the work of the creators!

- [Kenney's donation page (itch.io)](https://kenney.itch.io/kenney-donation)
- [Muffin Dev's BuyMeACoffee page](https://www.buymeacoffee.com/muffindev)